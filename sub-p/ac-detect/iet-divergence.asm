cli
xor     r8d, r8d
mov     ecx, IA32_APERF_MSR
rdmsr
shl     rdx, 20h
or      rax, rdx
mov     r9, rax
lea     rsi, [rsp+20h]
xor     eax, eax
cpuid
mov     [rsi], eax
mov     [rsi+4], ebx
mov     [rsi+8], ecx
mov     [rsi+0Ch], edx
mov     ecx, IA32_APERF_MSR
rdmsr
shl     rdx, 20h
or      rax, rdx
mov     rdx, [rsp+30h]
sub     rax, r9
mov     [rdx+r8*8], rax

;
; TODO:
; 	Capture comparable instruction IET.
; 	Store result.
; 	Loop. Break at end.
; 	Enable interrupts.
; 	End profile.
;

