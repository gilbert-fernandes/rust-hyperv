UINT64 XCR0 = _xgetbv(0);

__try {

    //
    // Clear the bit 0 of XCR0 to cause a #GP(0)!
    //
    _xsetbv(0, XCR0 & ~1);

} __except(EXCEPTION_EXECUTE_HANDLER) {

    //
    // If we get here, the host has properly handled XSETBV and injected a
    // #GP(0) into the guest.
    //
    LOG_DEBUG("1337!");
}

VMM_EVENT_STATUS HVAPI VmmHandleXsetbv(PVIRTUAL_CPU VirtualCpu)
{
    UINT32 Xcr;
    ULARGE_INTEGER XcrValue;

    Xcr = (UINT32)VirtualCpu->Context->Rcx;
    XcrValue.u.LowPart = (UINT32)VirtualCpu->Context->Rax;
    XcrValue.u.HighPart = (UINT32)VirtualCpu->Context->Rdx;

    //
    // Blindly execute XSETBV with whatever the guest gives us, because we
    // trust our guest :)
    //
    _xsetbv(Xcr, XcrValue.QuadPart);

    return VMM_HANDLED_ADVANCE_RIP;
}

KBUGCHECK_REASON_CALLBACK_RECORD BugCheckCallbackRecord = {0};
BOOLEAN BugCheckCallbackRegistered = FALSE;
static const UINT64 MagicNumber = 0x1337133713371337;
// b4911b81-7b73-4f2b-afcc-3b7ce3e1480c
static const GUID MagicDriverGuid = {0xb4911b81, 0x7b73, 0x4f2b, {0xaf, 0xcc, 0x3b, 0x7c, 0xe3, 0xe1, 0x48, 0x0c} };

VOID BugCheckCallbackRoutine(KBUGCHECK_CALLBACK_REASON Reason, struct _KBUGCHECK_REASON_CALLBACK_RECORD* Record, PVOID ReasonSpecificData, UINT32 ReasonSpecificDataLength)
{
    PKBUGCHECK_SECONDARY_DUMP_DATA SecondaryDumpData;
    SecondaryDumpData = (PKBUGCHECK_SECONDARY_DUMP_DATA)ReasonSpecificData;
    SecondaryDumpData->Guid = MagicDriverGuid;
    SecondaryDumpData->OutBuffer = (PVOID)&MagicNumber;
    SecondaryDumpData->OutBufferLength = sizeof(MagicNumber);
}

//
// ...
//

KeInitializeCallbackRecord(&BugCheckCallbackRecord);
BugCheckCallbackRegistered = KeRegisterBugCheckReasonCallback(&BugCheckCallbackRecord, BugCheckCallbackRoutine, KbCallbackSecondaryDumpData, (PUINT8)"secret.club");

if (!BugCheckCallbackRegistered) {
    return STATUS_UNSUCCESSFUL;
}

static BOOLEAN VmmpIsValidXcr0(UINT64 Xcr0)
{
    // FP must be unconditionally set.
    if (!(Xcr0 & X86_XCR0_FP)) {
        return FALSE;
    }

    // YMM depends on SSE.
    if ((Xcr0 & X86_XCR0_YMM) && !(Xcr0 & X86_XCR0_SSE)) {
        return FALSE;
    }

    // BNDREGS and BNDCSR must be the same.
    if ((!(Xcr0 & X86_XCR0_BNDREGS)) != (!(Xcr0 & X86_XCR0_BNDCSR))) {
        return FALSE;
    }

    // Validate AVX512 xsave feature bits.
    if (Xcr0 & X86_XSTATE_MASK_AVX512) {

        // OPMASK, ZMM, and HI_ZMM require YMM.
        if (!(Xcr0 & X86_XCR0_YMM)) {
            return FALSE;
        }

        // OPMASK, ZMM, and HI_ZMM must be the same.
        if (~Xcr0 & (X86_XCR0_OPMASK | X86_XCR0_ZMM | X86_XCR0_HI_ZMM)) {
            return FALSE;
        }
    }

    // XCR0 feature bits are valid!
    return TRUE;
}

VMM_EVENT_STATUS HVAPI VmmHandleXsetbv(PVIRTUAL_CPU VirtualCpu)
{
    UINT32 Xcr;
    ULARGE_INTEGER XcrValue;

    Xcr = (UINT32)VirtualCpu->Context->Rcx;

    // Make sure the guest is not trying to write to a bogus XCR.
    //
    switch(Xcr) {
    case X86_XCR_XFEATURE_ENABLED_MASK:
        break;

    default:
        HV_DBG_BREAK();
    GPFault:
        VmxInjectGP(VirtualCpu, 0);
        return VMM_NOT_HANDLED;
    }

    XcrValue.u.LowPart = (UINT32)VirtualCpu->Context->Rax;
    XcrValue.u.HighPart = (UINT32)VirtualCpu->Context->Rdx;

    // Make sure the guest is not trying to set any unsupported bits.
    //
    if (XcrValue.QuadPart & ~GetSupportedXcr0Bits()) {
        HV_DBG_BREAK();
        goto GPFault;
    }

    // Make sure bits being set are architecturally valid.
    //
    if (!VmmpIsValidXcr0(XcrValue.QuadPart)) {
        HV_DBG_BREAK();
        goto GPFault;
    }

    // By this point, the XCR value should be accepted by hardware.
    //
    _xsetbv(Xcr, XcrValue.QuadPart);

    return VMM_HANDLED_ADVANCE_RIP;
}


