BOOLEAN KiSyntheticMsrCheck(VOID)
{

#define HV_SYNTHETIC_MSR_RANGE_START 0x40000000
    
    __try
    {
        __readmsr( HV_SYNTHETIC_MSR_RANGE_START );
    } 
    __except(EXCEPTION_EXECUTE_HANDLER)
    {
        return FALSE;
    }
    
    return TRUE;
}

