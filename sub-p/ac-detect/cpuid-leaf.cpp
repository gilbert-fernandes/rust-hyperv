UINT64 UmpIsSystemVirtualized(void)
{
    unsigned int invalid_leaf = 0x13371337;
    unsigned int valid_leaf = 0x40000000;
    
    struct _HV_DETAILS
    {
        unsigned int Data[4];
    };
    
    _HV_DETAILS InvalidLeafResponse = { 0 };
    _HV_DETAILS ValidLeafResponse = { 0 };
  
    __cpuid( &InvalidLeafResponse, invalid_leaf );
    __cpuid( &ValidLeafResponse, valid_leaf );
  
    if( ( InvalidLeafResponse.Data[ 0 ] != ValidLeafResponse.Data[ 0 ] ) || 
        ( InvalidLeafResponse.Data[ 1 ] != ValidLeafResponse.Data[ 1 ] ) || 
        ( InvalidLeafResponse.Data[ 2 ] != ValidLeafResponse.Data[ 2 ] ) || 
        ( InvalidLeafResponse.Data[ 3 ] != ValidLeafResponse.Data[ 3 ] ) )
        return STATUS_HV_DETECTED;
    
    return STATUS_HV_NOT_PRESENT;
}

UINT64 UmpIsSystemVirtualized2(void)
{
    cpuid_buffer_t regs;
    __cpuid((int32_t*)&regs, 0x40000000);

    cpuid_buffer_t reserved_regs;
    __cpuid((int32_t*)&reserved_regs, 0);
    __cpuid((int32_t*)&reserved_regs, reserved_regs.eax);

    if (reserved_regs.eax != regs.eax || 
        reserved_regs.ebx != regs.ebx || 
        reserved_regs.ecx != regs.ecx || 
        reserved_regs.edx != regs.edx)
            return STATUS_HV_DETECTED;
            
    return STATUS_HV_NOT_PRESENT;
}

