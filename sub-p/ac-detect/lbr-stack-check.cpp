// Save current LBR top of stack
auto last_branch_taken_pre = __read_lbr_tos();

// Force VM-exit with CPUID
__cpuid(0, &regs);

// Save post VM-exit LBR top of stack
auto last_branch_taken_post = __read_lbr_tos();

// Compare last branch taken
if(last_branch_taken_pre != last_branch_taken_post)
    return TRUE;

// Last Branch Source Addresses
MSR_LASTBRANCH_0_FROM_IP
MSR_LASTBRANCH_N-1_FROM_IP
    
// Last Branch Target Addresses
MSR_LASTBRANCH_0_TO_IP
MSR_LASTBRANCH_N-1_TO_IP


